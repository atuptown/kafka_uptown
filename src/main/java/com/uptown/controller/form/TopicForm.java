package com.uptown.controller.form;

import lombok.Data;

/**
 * @Author lixiaofei
 * @create 2020/10/22 8:07 下午
 */
@Data
public class TopicForm {
    // 消费组
    private String groupId;
    // 监听的主题
    private String topic;
    // 生产者地址
    private String bootServer;

    private String filedId = "system_time";


    // 用作 线程名、rediskey、kafka组名
    // 保持一致 yaml中的key + 图表依赖工作表id + 业务字段或SYS_TIME
    public static String getOnlyKey(String redisKey, String tbId, String fieldId) {
        return redisKey + ":" + tbId + ":" + fieldId;
    }
}
