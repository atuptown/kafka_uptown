package com.uptown.mq.config;

import java.util.Objects;

import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import com.haizhi.hora.config.RedisConfig;

/**
 * Jedis工厂类（单例模式）
 */
@Service
public final class JedisFactory {


	public Jedis getJedis() {
		RedisConnection conn =  RedisConnectionUtils.getConnection(Objects.requireNonNull(
			RedisConfig.getRedisTemplate().getConnectionFactory()));
		return (Jedis) conn.getNativeConnection();
	}
}
