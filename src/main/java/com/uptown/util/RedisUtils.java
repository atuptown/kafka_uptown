package com.uptown.util;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * 基于spring和redis的redisTemplate工具类
 */
@Slf4j
public final class RedisUtils {



	// ============================zset=============================
	/**
	 * 根据key获取Set中的所有值
	 *
	 * @param key
	 *            键
	 * @return
	 */
	public static Set<Object> zSGet(String key) {
		try {
			return RedisConfig.getRedisTemplate().opsForSet().members(key);
		} catch (Exception e) {
			log.error(key, e);
			return null;
		}
	}

	/**
	 * 根据value从一个set中查询,是否存在
	 *
	 * @param key
	 *            键
	 * @param value
	 *            值
	 * @return true 存在 false不存在
	 */
	public static boolean zSHasKey(String key, Object value) {
		try {
			return RedisConfig.getRedisTemplate().opsForSet().isMember(key, value);
		} catch (Exception e) {
			log.error(key, e);
			return false;
		}
	}

	public static Boolean zSSet(String key, Object value, double score) {
		try {
			return RedisConfig.getRedisTemplate().opsForZSet().add(key, value, score);
		} catch (Exception e) {
			log.error(key, e);
			return false;
		}
	}

	public static Set<Object> zRangeByScoreWithScores(String key, Double start, Double end) {
		try {
			return RedisConfig.getRedisTemplate().opsForZSet().rangeByScoreWithScores(key, start, end);
		} catch (Exception e) {
			log.error(key, e);
			return null;
		}
	}

	/**
	 * 获取set缓存的长度
	 *
	 * @param key
	 *            键
	 * @return
	 */
	public static long zSGetSetSize(String key) {
		try {
			return RedisConfig.getRedisTemplate().opsForSet().size(key);
		} catch (Exception e) {
			log.error(key, e);
			return 0;
		}
	}

	/**
	 * 移除值为value的
	 *
	 * @param key
	 *            键
	 * @param values
	 *            值 可以是多个
	 * @return 移除的个数
	 */
	public static long zSetRemove(String key, Object... values) {
		try {
			Long count = RedisConfig.getRedisTemplate().opsForSet().remove(key, values);
			return count;
		} catch (Exception e) {
			log.error(key, e);
			return 0;
		}
	}



	/**
	 * 获取list缓存的内容
	 *
	 * @param key   键
	 * @param start 开始
	 * @param end   结束 0 到 -1代表所有值
	 * @return
	 */
	@SneakyThrows
	public static List<Object> lGet(String key, long start, long end) {
		try {
			return RedisConfig.getRedisTemplate().opsForList().range(key, start, end);
		} catch (Exception e) {
			log.error(key, e);
			throw new Exception(e);
		}
	}

	/**
	 * 获取list缓存的长度
	 *
	 * @param key 键
	 * @return
	 */
	@SneakyThrows
	public static long lGetListSize(String key) {
		try {
			return RedisConfig.getRedisTemplate().opsForList().size(key);
		} catch (Exception e) {
			log.error(key, e);
			throw new Exception(e);
		}
	}

	/**
	 * 通过索引 获取list中的值
	 *
	 * @param key   键
	 * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
	 * @return
	 */
	@SneakyThrows
	public static Object lGetIndex(String key, long index) {
		try {
			return RedisConfig.getRedisTemplate().opsForList().index(key, index);
		} catch (Exception e) {
			log.error(key, e);
			throw new Exception(e);
		}
	}

	/**
	 * 将list放入缓存
	 *
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public static void lSet(String key, Object value) {
		try {
			RedisConfig.getRedisTemplate().opsForList().rightPush(key, value);
		} catch (Exception e) {
			log.error(key, e);
		}
	}

	/**
	 * 将list放入缓存
	 *
	 * @param key   键
	 * @param value 值
	 * @param time  时间(秒)
	 * @return
	 */
	public static void lSet(String key, Object value, long time) {
		try {
			RedisConfig.getRedisTemplate().opsForList().rightPush(key, value);
			if (time > 0) {
				expire(key, time);
			}
		} catch (Exception e) {
			log.error(key, e);
		}
	}

	/**
	 * 将list放入缓存
	 *
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public static void lSet(String key, List<Object> value) {
		try {
			RedisConfig.getRedisTemplate().opsForList().rightPushAll(key, value);
		} catch (Exception e) {
			log.error(key, e);
		}
	}

	/**
	 * 将list放入缓存
	 *
	 * @param key   键
	 * @param value 值
	 * @param time  时间(秒)
	 * @return
	 */
	public static void lSet(String key, List<Object> value, long time) {
		try {
			RedisConfig.getRedisTemplate().opsForList().rightPushAll(key, value);
			if (time > 0) {
				expire(key, time);
			}
		} catch (Exception e) {
			log.error(key, e);
		}
	}

	/**
	 * 根据索引修改list中的某条数据
	 *
	 * @param key   键
	 * @param index 索引
	 * @param value 值
	 * @return
	 */
	public static void lUpdateIndex(String key, long index, Object value) {
		try {
			RedisConfig.getRedisTemplate().opsForList().set(key, index, value);
		} catch (Exception e) {
			log.error(key, e);
		}
	}

	/**
	 * 移除N个值为value
	 *
	 * @param key   键
	 * @param count 移除多少个
	 * @param value 值
	 * @return 移除的个数
	 */
	@SneakyThrows
	public static long lRemove(String key, long count, Object value) {
		try {
			Long remove = RedisConfig.getRedisTemplate().opsForList().remove(key, count, value);
			return remove;
		} catch (Exception e) {
			log.error(key, e);
			throw new Exception(e);
		}
	}

	public static void expire(String key, long time) {
		try {
			if (time > 0) {
				RedisConfig.getRedisTemplate().expire(key, time, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			log.error(key, e);
		}
	}

}

